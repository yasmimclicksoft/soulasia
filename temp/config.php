<?php
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);
// HTTP
define('HTTP_SERVER', 'http://localhost/soulasia/temp/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/soulasia/temp/');

// DIR
define('DIR_APPLICATION', '/var/www/html/soulasia/temp/catalog/');
define('DIR_SYSTEM', '/var/www/html/soulasia/temp/system/');
define('DIR_DATABASE', '/var/www/html/soulasia/temp/system/database/');
define('DIR_LANGUAGE', '/var/www/html/soulasia/temp/catalog/language/');
define('DIR_TEMPLATE', '/var/www/html/soulasia/temp/catalog/view/theme/');
define('DIR_CONFIG', '/var/www/html/soulasia/temp/system/config/');
define('DIR_IMAGE', '/var/www/html/soulasia/temp/image/');
define('DIR_CACHE', '/var/www/html/soulasia/temp/system/cache/');
define('DIR_DOWNLOAD', '/var/www/html/soulasia/temp/download/');
define('DIR_LOGS', '/var/www/html/soulasia/temp/system/logs/');

// DB
define('DB_DRIVER', 'mysqli');
/*
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'soulasia_admin');
define('DB_PASSWORD', '3M!Cy|maYx1B');
define('DB_DATABASE', 'soulasia_db');*/
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'clicksof_soulasia');
define('DB_PORT', '3306');

define('DB_PREFIX', 'oc_');
?>